# Noiseless Tiger

Usually at the command prompt the main actor is the command to execute, it reigns over its parameters. What is more important, the command or the object (machine, service, etc) you need to manage?. If you opt for the second choice this program is for you!

## Usage

`# object command parameters`

where
* object : machine, service or any object to manage
* action : command to manage objects
* parameters : guess what? :D

e.g.
```
# machine-name shutdown
# service stop
# my-server connect
```

## Setup

* Change `command_not_found_handle` function to invoke `nt` (noiseless tiger) command when command not found
* Set environment variable `NT_ACTIONS` to your actions folder

### Handle command not found

Invoking `nt` command at the command line (e.g. `nt my-server shutdown`) is neither sexy nor put the emphasis at the object to manage. To achieve this use bash `command_not_found_handle` (or your preferred shell mechanism to handle command not found) to invoke `nt` when a command is not found. This will convert `my-server shutdown` into command `nt my-server shutdown` which will validate `my-server` name against your machine `/etc/hosts` file

Just to make it work modify the `/etc/bash.bashrc` file at your installation (location may vary according to different Linux distros) to invoke `nt` command. This is how I made the magic happen (diff file follows) :

```
--- bash.bashrc	2021-05-09 18:09:30.798729347 -0300
+++ /etc/bash.bashrc	2021-05-09 18:20:43.554069178 -0300
@@ -40,9 +40,13 @@
 #  fi
 #fi
 
-# if the command-not-found package is installed, use it
-if [ -x /usr/lib/command-not-found -o -x /usr/share/command-not-found/command-not-found ]; then
+# Install command-not-found handler
 	function command_not_found_handle {
+                if [ -x $HOME/noiseless-tiger/bin/nt ]; then
+                   $HOME/noiseless-tiger/bin/nt $@
+                   [[ $? -eq 0 ]] && return $?
+                fi
+
 	        # check because c-n-f could've been removed in the meantime
                 if [ -x /usr/lib/command-not-found ]; then
 		   /usr/lib/command-not-found -- "$1"
@@ -55,4 +59,3 @@
 		   return 127
 		fi
 	}
-fi
```

### Create your own actions

There's no actions bundled into this repository (batteries not included!) then you have to create your own that will be invoked as actions. Set the environment variable `NT_ACTIONS` to the folder where your actions are located.

Let's say that want to create action `connect` then create the next shell script :

```
#!/usr/bin/env bash

Host=$1

ssh -X $Host
```

Name it `connect`, save it under `$HOME/my-pretty-actions` and make it executable. Then set the environment variable `NT_ACTIONS` to `$HOME/my-pretty-actions`. 

Voilà! running `my-server connect` will ssh to it